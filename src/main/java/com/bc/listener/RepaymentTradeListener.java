package com.bc.listener;

import com.bc.event.RepaymentEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


/**
 * 还款监听器
 */
@Component
public class RepaymentTradeListener implements ApplicationListener<RepaymentEvent> {

    private Logger logger = LoggerFactory.getLogger(RepaymentTradeListener.class);

    @Async
    @Override
    public void onApplicationEvent(RepaymentEvent repaymentEvent) {
        logger.info("还款交易监听器{}",repaymentEvent);

    }
}
