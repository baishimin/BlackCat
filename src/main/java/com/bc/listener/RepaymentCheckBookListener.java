package com.bc.listener;

import com.bc.event.RepaymentEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 还款核销
 */
@Component
public class RepaymentCheckBookListener implements ApplicationListener<RepaymentEvent> {

    private Logger logger = LoggerFactory.getLogger(RepaymentCheckBookListener.class);

    @Async
    @Override
    public void onApplicationEvent(RepaymentEvent repaymentEvent) {
        logger.info("还款核销监听器{}",repaymentEvent);

    }
}
