package com.bc.listener;

import com.bc.event.RepaymentRegisterEvent;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 送积分 消息总线监听器
 */
@Component
public class IntegralEventListener {

    private Logger logger = LoggerFactory.getLogger(IntegralEventListener.class);

    @Autowired
    private AsyncEventBus asyncEventBus;

    // 注册该类
    @PostConstruct
    public void register(){
        asyncEventBus.register(this);
    }

    @Subscribe
    public void listen(RepaymentRegisterEvent event){
        //送积分
        logger.info("送积分: "+event.getMessage());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {

        }finally {

            logger.info("送积分 end");
        }
    }

    @Subscribe
    public void listen(String message){
        logger.info("receive 2 message: "+message);
    }

}
