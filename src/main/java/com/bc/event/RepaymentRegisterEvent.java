package com.bc.event;

/**
 * Guava 定义还款事件
 */
public class RepaymentRegisterEvent {

    public RepaymentRegisterEvent(String message) {
        this.message = message;
    }

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
