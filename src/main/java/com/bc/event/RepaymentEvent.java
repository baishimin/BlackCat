package com.bc.event;

import org.springframework.context.ApplicationEvent;

/**
 * 还款事件源
 */
public class RepaymentEvent  extends ApplicationEvent {

    private static final long serialVersionUID = -7551887340758222587L;

    public RepaymentEvent(Object source) {
        super(source);
    }

    public RepaymentEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    /**
     * 消息
     */
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "RepaymentEvent{" +
                "message='" + message + '\'' +
                '}';
    }
}
