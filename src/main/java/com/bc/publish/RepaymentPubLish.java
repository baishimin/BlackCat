package com.bc.publish;

import com.bc.event.RepaymentEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * 时间发布
 */
@Component
public class RepaymentPubLish {


    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    /**
     * 还款事件发布
     * @param message
     */
    public void repaymentPublish(String message) {
        RepaymentEvent demo = new RepaymentEvent(this, message);
        applicationEventPublisher.publishEvent(demo);
    }
}
