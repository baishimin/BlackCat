package com.bc.config;

import com.google.common.eventbus.AsyncEventBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 事件注入中心
 */
@Configuration
@EnableAsync
public class AsynEventBusConfig {

    @Bean
    @Scope("singleton")
    public AsyncEventBus asyncEventBus() {
        final ThreadPoolTaskExecutor executor = executor();
        return new AsyncEventBus(executor);
    }

    @Bean
    public ThreadPoolTaskExecutor executor(){
        /*
        org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
        private int corePoolSize = 1;
        private int maxPoolSize = 2147483647;
        private int queueCapacity = 2147483647;
        private int keepAliveSeconds = 60;
        private boolean allowCoreThreadTimeOut = false;
        * */
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //线程池的基本大小
        executor.setCorePoolSize(30);
        //线程池最大线程数量
        executor.setMaxPoolSize(30);
        //线程池所使用的缓冲队列
        executor.setQueueCapacity(1000);
        ////线程池维护线程所允许的空闲时间
        executor.setKeepAliveSeconds(600);
        // executor.setAllowCoreThreadTimeOut(true);
        return executor;
    }
}
