package com.bc.exception;

/**
 * 业务异常信息
 */
public class BizException extends Exception {

    private String status;

    public BizException(String errmsg) {
        super(errmsg);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
