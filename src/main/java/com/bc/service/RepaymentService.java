package com.bc.service;

import com.bc.exception.BizException;
import com.bc.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RepaymentService {

    private final Logger logger = LoggerFactory.getLogger(RepaymentService.class);

    public void repaymentCheck(String subjectCode) throws BizException,ServiceException {

            if (subjectCode.equals("本金")){
                logger.info("还款成功！");
            }else{
                throw new BizException("失败");
            }
            throw new ServiceException("超时");

    }
}
