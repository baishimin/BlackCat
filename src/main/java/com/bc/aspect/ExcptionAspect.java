package com.bc.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
@Aspect
@Order(2)
public class ExcptionAspect {

    private final Logger logger = LoggerFactory.getLogger(ExcptionAspect.class);

    //连接点是@RequestMapping注解的方法
    @Pointcut(value = "execution(* com.bc.service.*.*(..))")
    private void servicePointCut() {
        logger.info("ExcptionAspect.servicePointCut 还款");
    }

    @AfterThrowing(pointcut = "servicePointCut()",throwing = "e")
    public void permissionCheck(JoinPoint joinPoint,Throwable e) {

        logger.info("ExcptionAspect.permissionCheck Exception");
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        //开始打log
        System.out.println("异常:" + e.getMessage());
        System.out.println("异常所在类：" + className);
        System.out.println("异常所在方法：" + methodName);
        System.out.println("异常中的参数：");
        for (int i = 0; i < args.length; i++) {
            System.out.println("入参 args:" + args[i].toString());
        }
    }


}
