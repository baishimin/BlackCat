package com.bc;

import com.bc.event.RepaymentRegisterEvent;
import com.bc.exception.BizException;
import com.bc.publish.RepaymentPubLish;
import com.bc.service.RepaymentService;
import com.google.common.eventbus.AsyncEventBus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BlackcatApplicationTests {

    @Autowired
    private RepaymentPubLish pubLish;

    @Autowired
    private AsyncEventBus asyncEventBus;

    @Autowired
    private RepaymentService repaymentService;

    @Test
    public void contextLoads() {
        for (int i = 0; i < 50; i++) {
            //同步
            System.out.println("====start "+ i +"次 ====");
            pubLish.repaymentPublish("本金");
            System.out.println("====end====");

        }
    }


    @Test
    public void eventBusTest(){
        for (int i = 0; i < 59; i++) {
            System.out.println("====start "+ i +"次 ====");
            RepaymentRegisterEvent repaymentRegisterEvent = new RepaymentRegisterEvent("还款");
            asyncEventBus.post(repaymentRegisterEvent);
            System.out.println("====end====");

        }
    }

    @Test
    public void eventBusMessageTest(){

        asyncEventBus.post("测试");
    }


    @Test
    public void repaymentTest() throws BizException {

            repaymentService.repaymentCheck("本金");

    }
}
